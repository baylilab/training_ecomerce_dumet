    <div class="product-big-title-area"> 
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Shopping Cart</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End Page title area -->
    
    
    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Search Products</h2>
                        <form action="#">
                            <input type="text" placeholder="Search products...">
                            <input type="submit" value="Search">
                        </form>
                    </div>
                    
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Products</h2>
                        <?php foreach($latest_product as $latest_products): ?>
                        <div class="thubmnail-recent">
                            <img src="<?php echo base_url() ?>img/<?php echo $latest_products->thumbnail ?>" class="recent-thumb" alt="">
                            <h2><a href="<?php echo site_url('HomeController/productDetail/'.$latest_products->id); ?>"><?php echo $latest_products->product_name ?></a></h2>
                            <div class="product-sidebar-price">
                                <ins>Rp.<?php echo $latest_products->price ?></ins> <del>Rp.<?php echo $latest_products->price_before ?></del>
                            </div>                             
                        </div>
                    <?php endforeach; ?>
                        
                    </div>
                    
                   
                </div>
                
                <div class="col-md-8">
                    <div class="product-content-right">
                        <div class="woocommerce">
                            <form method="POST" action="<?php echo site_url('ShoppingCartController/editCartItem/') ?>">
                                <table cellspacing="0" class="shop_table cart">
                                    <thead>
                                        <tr>
                                            <th class="product-remove">&nbsp;</th>
                                            <th class="product-thumbnail">&nbsp;</th>
                                            <th class="product-name">Product</th>
                                            <th class="product-price">Price</th>
                                            <th class="product-quantity">Quantity</th>
                                            <th class="product-subtotal">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(empty($cart_content)) :?>
                                            <h3>Barang belanjaan anda kosong</h3>
                                        <?php else : ?>
                                        <?php foreach($cart_content as $cart_contents) : ?>
                                        <tr class="cart_item">
                                            <td class="product-remove">
                                                <a title="Remove this item" class="remove" href="<?php echo site_url('ShoppingCartController/removeCartItem/'.$cart_contents['rowid']) ?>">×</a> 
                                            </td>

                                            <td class="product-thumbnail">
                                                <a href="single-product.html"><img width="145" height="145" alt="poster_1_up" class="shop_thumbnail" src="<?php echo base_url('img/'.$cart_contents['img']) ?>"></a>
                                            </td>

                                            <td class="product-name">
                                                <a href="single-product.html"><?php echo $cart_contents['name'] ?></a> 
                                            </td>

                                            <td class="product-price">
                                                <span class="amount">Rp.<?php echo $cart_contents['price'] ?></span> 
                                            </td>

                                            <td class="product-quantity">
                                                <div class="quantity buttons_added">
                                                    <input type="button" class="minus" value="-">
                                                    <input type="number" name="qty[]" size="4" class="input-text qty text" title="Qty" value="<?php echo $cart_contents['qty'] ?>" min="0" step="1">
                                                    <input type="button" class="plus" value="+">
                                                     <input type="hidden" name="rowid[]" value="<?php echo $cart_contents['rowid']?>">
                                                </div>
                                            </td>

                                            <td class="product-subtotal">
                                                <span class="amount">Rp.<?php echo ($cart_contents['price']*$cart_contents['qty']) ?></span> 
                                            </td>
                                        </tr>

                                              <?php endforeach; ?>
                                            <?php endif; ?>
                                        <tr>
                                            <td class="actions" colspan="6">
                                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                                                <input type="submit" value="Update Cart" name="update_cart" class="button" 
                                                <?php $a =  (empty($cart_content)) ?  "disabled = 'disabled'" : ""; ?>
                                                <?php echo $a; ?>
                                                >
                                                <input type="submit" value="Checkout" name="proceed" class="checkout-button button alt wc-forward" 
                                                <?php $a =  (empty($cart_content)) ?  "disabled = 'disabled'" : ""; ?>
                                                <?php echo $a; ?>
                                                >
                                            </td>
                                        </tr>

                                  
                                    </tbody>
                                </table>
                            </form>

                            <div class="cart-collaterals">


                            <div class="cross-sells">
                                <h2>You may be interested in...</h2>
                                <ul class="products">
                                    <?php foreach($low_price_product as $low_price_products): ?>
                                    <li class="product">
                                        <a href="single-product.html">
                                            <img width="325" height="325" alt="T_4_front" class="attachment-shop_catalog wp-post-image" src="<?php echo base_url() ?>img/<?php echo $low_price_products->thumbnail?>">
                                            <h3><?php echo $low_price_products->product_name ?></h3>
                                            <span class="price"><span class="amount">Rp.<?php echo $low_price_products->price ?></span></span>
                                        </a>

                                        <a class="add_to_cart_button" data-quantity="1" data-product_sku="" data-product_id="22" rel="nofollow" href="<?php echo site_url('ShoppingCartController/directAddToCart/'.$low_price_products->id) ?>">Select options</a>
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                                </ul>
                            </div>


                            <div class="cart_totals ">
                                <h2>Cart Totals</h2>

                                <table cellspacing="0">
                                    <tbody>
                                        <tr class="cart-subtotal">
                                            <th>Cart Subtotal</th>
                                            <td><span class="amount">Rp.<?php echo $cart_total ?></span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>                        
                    </div>                    
                </div>
            </div>
        </div>
    </div>