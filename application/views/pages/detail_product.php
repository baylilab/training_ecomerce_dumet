    <div class="product-big-title-area"> 
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Shop</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Search Products</h2>
                        <form action="">
                            <input type="text" placeholder="Search products...">
                            <input type="submit" value="Search">
                        </form>
                    </div>
                    
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Products</h2>
                        <?php foreach($latest_product as $latest_products): ?>
                        <div class="thubmnail-recent">
                            <img src="" class="recent-thumb" alt="">
                            <h2><a href="<?php echo site_url('homecontroller/productDetail/'.$latest_products->id); ?>"><?php echo $latest_products->product_name ?></a></h2>
                            <div class="product-sidebar-price">
                                <ins>Rp.<?php echo $latest_products->price ?></ins> <del>Rp.<?php echo $latest_products->price_before ?></del>
                            </div>                             
                        </div>
                    <?php endforeach; ?>
                    </div>
                </div>
                
                <div class="col-md-8">
                    <?php if(empty($product_detail)) : ?>
                    <h3>Sory Product not found</h3>
                    <?php else:?>
                    <div class="product-content-right">
                        <div class="product-breadcroumb">
                            <a href="">Home</a>
                            <a href="">Category Name</a>
                            <a href="">Sony Smart TV - 2015</a>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="product-images">
                                    <div class="product-main-img">
                                        <img src="<?php echo site_url('img/'.$product_detail->thumbnail) ?>" alt="">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6">
                                <div class="product-inner">
                                    <h2 class="product-name"><?php echo $product_detail->product_name ?></h2>
                                    <div class="product-inner-price">
                                        <ins>Rp.<?php echo $product_detail->price ?></ins> <del>Rp.<?php echo $product_detail->price_before ?></del>
                                    </div>    
                                    
                                    <form action="<?php echo site_url('ShoppingCartController/addtocartwithform')?>" method="POST" class="cart">
                                        <div class="quantity">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                                            <input type="hidden" size="4" class="input-text qty text" title="" value="<?php echo $product_detail->id ?>" name="id" >
                                            <input type="number" size="4" class="input-text qty text" title="Qty" value="1" name="qty" min="1" step="1">
                                        </div>
                                        <button class="add_to_cart_button" type="submit">Add to cart</button>
                                    </form>   
                                    
                                    <div class="product-inner-category">
                                        <p>Category: <a href="">Summer</a>. Tags: <a href="">awesome</a>, <a href="">best</a>, <a href="">sale</a>, <a href="">shoes</a>. </p>
                                    </div> 
                                    
                                    <div role="tabpanel">
                                        <ul class="product-tab" role="tablist">
                                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Description</a></li>
                                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Reviews</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade in active" id="home">
                                                <h2>Product Description</h2>  
                                                <p><?php echo $product_detail->description ?></p>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="profile">
                                                <h2>Reviews</h2>
                                                <div class="submit-review">
                                                    <p><label for="name">Name</label> <input name="name" type="text"></p>
                                                    <p><label for="email">Email</label> <input name="email" type="email"></p>
                                                    <div class="rating-chooser">
                                                        <p>Your rating</p>

                                                        <div class="rating-wrap-post">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                    </div>
                                                    <p><label for="review">Your review</label> <textarea name="review" id="" cols="30" rows="10"></textarea></p>
                                                    <p><input type="submit" value="Submit"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="related-products-wrapper">
                            <h2 class="related-products-title">Related Products</h2>
                            <div class="related-products-carousel">
                              <?php foreach($product_related as $product_related) :?>  
                                <div class="single-product">
                                    <div class="product-f-image">
                                        <img src="<?php echo base_url() ?>img/<?php echo $product_related->thumbnail ?>" alt="">
                                        <div class="product-hover">
                                            <a href="<?php echo site_url('ShoppingCartController/directAddToCart/'.$product_related->id) ?>" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                            <a href="<?php echo site_url('HomeController/productDetail/'.$product_related->id); ?>" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                                        </div>
                                    </div>

                                    <h2><a href=""><?php echo $product_related->product_name ?></a></h2>

                                    <div class="product-carousel-price">
                                        <ins>Rp.<?php echo $product_related->price ?></ins> <del>Rp.<?php echo $product_related->price_before ?></del>
                                    </div> 
                                </div>

                                  <?php endforeach; ?>
                            </div>
                        </div>
                    </div>    

                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>