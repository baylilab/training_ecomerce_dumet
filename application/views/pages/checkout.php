
 <div class="product-big-title-area"> 
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Shopping Cart</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Search Products</h2>
                        <form action="">
                            <input type="text" placeholder="Search products...">
                            <input type="submit" value="Search">
                        </form>
                    </div>
                    
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Products</h2>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                    </div>
                </div>
                
                <div class="col-md-8">
                    <div class="product-content-right">
                        <div class="woocommerce">

                            <?php if($this->session->userdata('logged_in') !== TRUE) :?>
                            <div class="woocommerce-info">Sudah punya akun? <a class="showlogin" data-toggle="collapse" href="#login-form-wrap" aria-expanded="false" aria-controls="login-form-wrap">Silahkan Login Di Sini</a>
                            </div>

                            <form id="login-form-wrap" action="<?php echo site_url('/auth/MemberLoginController/login') ?>" class="login collapse" method="post">

                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">


                                <p>JIka anda telah menjadi member kami silahkan login terlebih dahulu, jika belum silahkan lakukan registrasi.</p>

                                <p class="form-row form-row-first">
                                    <label for="username">Username or email <span class="required">*</span>
                                    </label>
                                    <input type="text" id="username" name="username" class="input-text">
                                </p>
                                <p class="form-row form-row-last">
                                    <label for="password">Password <span class="required">*</span>
                                    </label>
                                    <input type="password" id="password" name="password" class="input-text">
                                </p>
                                <div class="clear"></div>
                                <p>
                                    <button type="submit" class="btn btn-sm btn-primary">Login</button>
                                </p>
                                </form>
                                <p class="lost_password">
                                    <a href="#" data-toggle="modal" data-target="#modal-register-form">Belum punya akun?</a>
                                </p>

                                <div class="clear"></div>

                                <!-- Modal -->
                            <div id="modal-register-form" class="modal fade" role="dialog">
                              <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Akun Registrasi</h4>
                                  </div>
                                  
                                  <div class="modal-body">
                                  <form method="POST" action="<?php echo site_url('auth/MemberRegisterController/register/') ?>">
                                           <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                                            
                                                <label>Name</label>
                                                <input type="text" value="" placeholder="Name"  name="name" class="form-control">
                                         

                                         
                                                <label>Username</label>
                                                <input type="text" value="" placeholder="Username"  name="username" class="form-control">
                                      

                                       
                                                <label>Password</label>
                                                <input type="text" value="" placeholder="Password"  name="password" class="form-control">
                                       
                                      
                                                <label>Email</label>
                                                <input type="email" value="" placeholder="Email"  name="email" class="form-control">
                                    


                                                <label>Telp</label>
                                                <input type="text" value="" placeholder="Telp"  name="telp" class="form-control">
                                      

                                      
                                                <label>Alamat</label>
                                                <textarea name="shipping_address" class="form-control" placeholder="Alamat"></textarea><br>
                                          
                                                <button type="submit" name="member_register" class="btn btn-primary">Registrasi</button>
                                </form>
                                  </div>
                                 

                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  </div>
                                </div>

                              </div>
                            </div>
                    

                        <?php endif; ?>

                            <form enctype="multipart/form-data" action="#" class="checkout" method="post" name="checkout">

                                <div id="customer_details" class="col2-set">
                                    <div class="col-1">
                                        <div class="woocommerce-billing-fields">
                                            <h3>Billing Details</h3>
                                         
                                            <p id="billing_first_name_field" class="form-row form-row-first validate-required">
                                                <label class="" for="billing_first_name">Nama<abbr title="required" class="required">*</abbr>
                                                </label>
                                                <input type="text" value="<?php $a =  (!empty($data_member)) ?  $data_member->member_name : ""; ?><?php echo $a; ?>" placeholder="" id="billing_first_name" name="billing_first_name" class="input-text ">
                                            </p>

                                            <div class="clear"></div>

                                            <p id="billing_company_field" class="form-row form-row-wide">
                                                <label class="" for="billing_company">Email</label>
                                                <input type="email" value="<?php $a =  (!empty($data_member)) ?  $data_member->email : ""; ?><?php echo $a; ?>" placeholder="" id="billing_company" name="billing_company" class="form-control">
                                            </p>


                                            <p id="billing_city_field" class="form-row form-row-wide address-field validate-required" data-o_class="form-row form-row-wide address-field validate-required">
                                                <label class="" for="billing_city">Telp<abbr title="required" class="required">*</abbr>
                                                </label>
                                                <input type="text" value="<?php $a =  (!empty($data_member)) ?  $data_member->telp : ""; ?><?php echo $a; ?>" placeholder="" id="billing_city" name="billing_city" class="input-text ">
                                            </p>

                                            <p id="billing_address_1_field" class="form-row form-row-wide address-field validate-required">
                                                <label class="" for="billing_address_1">Alamat <abbr title="required" class="required">*</abbr>
                                                </label>
                                                <textarea name="shipping_address" placeholder="Alamat"><?php $a =  (!empty($data_member)) ?  $data_member->shipping_address : ""; ?><?php echo $a; ?></textarea>
                                            </p>

                                        </div>
                                    </div>

                                    <div class="col-2">
                                        
                                        <h2><a class="shipping-calculator-button" data-toggle="collapse" href="#calcalute-shipping-wrap" aria-expanded="false" aria-controls="calcalute-shipping-wrap">Calculate Shipping</a></h2>

                                        <section id="calcalute-shipping-wrap" class="shipping-calculator-form collapse">

                                            <p class="form-row form-row-wide">
                                             <select rel="calc_shipping_state" class="courier" id="courier" name="">
                                            <option value="">Pilih Jasa Ongkir</option>
                                            <option value="jne">JNE</option>
                                            <option value="tiki">TIKI</option>
                                            <option value="pos">POS</option>

                                           </select>
                                           </p>

                                        <p class="form-row form-row-wide">
                                        <select rel="calc_shipping_state" class="country_to_state" id="calc_shipping_country" name="calc_shipping_country">
                                            <option value="">Pilih Provinsi</option>
                                            <?php foreach($provinces->rajaongkir->results as $prov) : ?>
                                            <option value="<?php echo $prov->province_id ?>"><?php echo $prov->province ?></option>
                                            <?php endforeach;?>

                                        </select>
                                        </p>

                                        <p class="form-row form-row-wide">
                                             <select rel="calc_shipping_state" class="" id="city_by_prov_id" name="">
                                              <option value="">Pilih Kota</option>

                                           </select>
                                       </p>
                                       <p class="form-row form-row-wide">
                                            <input type="text" id="total_weight" value="<?php echo $total_wight ?>" disabled>
                                       </p>
                                       <p>
                                            <button type="button" name="cek_ongkir">Cek Ongkos Kirim</button>
                                       </p>

                                        </section>
                            

                                    </div>

                                </div>



                                <h3 id="order_review_heading">Your order</h3>

                                <div id="my_cost">
                               
                                </div>

                                <div id="order_review" style="position: relative;">
                                    <table class="shop_table">
                                        <thead>
                                            <tr>
                                                <th class="product-name">Product</th>
                                                <th class="product-total">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($cart_contents as $cart) : ?>
                                            <tr class="cart_item">
                                                <td class="product-name">
                                                   <?php echo $cart['name'] ?> <strong class="product-quantity">× <?php echo $cart['qty'] ?></strong> </td>
                                                <td class="product-total">
                                                    <span class="amount">Rp.<?php echo number_format($cart['price']*$cart['qty']) ?></span> </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                        <tfoot>

                                            <tr class="cart-subtotal">
                                                <th>Cart Subtotal</th>
                                                <td><span class="amount">Rp.<?php echo number_format($cart_sub_total) ?></span>
                                                </td>
                                            </tr>

                                            <tr class="shipping">
                                                <th>Shipping and Handling</th>
                                                <td id="total-cost-display">

                                                    Pilih courier Service
                                                   
                                                </td>
                                            </tr>


                                            <tr class="order-total">
                                                <th>Order Total</th>
                                                <td id="total"><strong><span class="amount"><input type="hidden" id="sub_total" value="<?php echo $this->cart->total() ?>">Rp.<?php echo $this->cart->total() ?></span></strong> </td>
                                            </tr>

                                        </tfoot>
                                    </table>


                                    <div id="payment">
                                        <ul class="payment_methods methods">
                                            <li class="payment_method_bacs">
                                                <input type="radio" data-order_button_text=""  value="bacs" name="payment_method" class="input-radio" id="payment_method_bacs">
                                                <label for="payment_method_bacs">Direct Bank Transfer </label>
                                                <div class="payment_box payment_method_bacs">
                                                    <p>Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</p>
                                                </div>
                                            </li>
                                        </ul>

                                        <div class="form-row place-order">

                                            <input type="submit" data-value="Place order" value="Place order" id="place_order" name="woocommerce_checkout_place_order" class="button alt">


                                        </div>

                                        <div class="clear"></div>

                                    </div>
                                </div>
                            </form>

                        </div>                       
                    </div>                    
                </div>
            </div>
        </div>
    </div>
