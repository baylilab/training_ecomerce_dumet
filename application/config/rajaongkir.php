<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Andi Siswanto <andisis92@gmail.com>
 * RajaOngkir API Key
 * Silakan daftar akun di RajaOngkir.com untuk mendapatkan API Key
 * http://rajaongkir.com/akun/daftar
 */
$config['rajaongkir_api_key'] = "523f7b70a5d640c1a5d72e4d790ade2c";

/**
 * RajaOngkir account type: starter / basic / pro
 * http://rajaongkir.com/dokumentasi#akun-ringkasan
 * 
 */
$config['rajaongkir_account_type'] = "starter";
