<?php


class Home extends CI_Model
{
	public function getProducts()
	{
		return $this->db->order_by('id','DESC')->get('product')->result();
	}

	public function getProductDetail($id)
	{
		return $this->db->get_where('product', array('id' => $id))->row();
	}

	public function relatedProductDetail($id, $id_cat)
	{

		return $this->db->get_where('product', array('category_id' => $id_cat, 'id !=' => $id))->result();
        
	}
	public function latestProduct()
	{
		return $this->db->order_by('id','DESC')->limit(4)->get('product')->result();
	}

	public function lowPriceProduct()
	{
		return $this->db->order_by('price', 'asc')->limit(2)->get('product')->result();
	}
}