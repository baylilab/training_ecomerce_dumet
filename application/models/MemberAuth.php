<?php

/**
 * summary
 */
class MemberAuth extends CI_Model
{
    /**
     * summary
     */
   public function get_member_for_login($username = NULL)
   {
   	  return $this->db->get_where('member', array('username' => $username))->row();
   }

   public function get_memeber_by_id($id_member = null)
   {
   	return $this->db->get_where('member', array('id' => $id_member))->row();
   }

   public function memberRegister($data)
   {
     return  $this->db->insert('member', $data);
   }

   public function changeActivation($token)
   {
    
    $this->db->where('member_token',$token);
    $this->db->update('member', array('activation' => 1));
    return true;
        
   }
   
}