<?php

class HomeController extends My_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Home');
	}

	public function index()
	{
		$page_path = "pages/home";
		$product  = $this->Home->getProducts();
		
		$this->load->view('base', compact('page_path','product'));
	}

	public function productDetail($id)
	{
		$page_path       = "pages/detail_product";
		$product_detail  = $this->Home->getProductDetail($id);
		$id_cat          = $product_detail->category_id;
		$product_related = $this->Home->relatedProductDetail($id,$id_cat);
		$latest_product  = $this->Home->latestProduct();

		$this->load->view('base', compact('page_path','product_detail','product_related','latest_product'));
	}

}