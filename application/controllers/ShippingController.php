<?php

class ShippingController extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('RajaOngkir');
    }

    public function getCityByProvid($prov_id)
    {
     
        $city = json_decode($this->rajaongkir->city($prov_id, NULL));
        
        $output = '<option>Pilih Kota</option>';
        foreach($city->rajaongkir->results as $city_by_prov_id){
            $output .='<option value="'.$city_by_prov_id->city_id.'">'.$city_by_prov_id->city_name.'</option>';
        }

        echo $output;
       
    }

    public function getCost()
    {
        $courier = $this->input->get('courier');
        $city_destination = $this->input->get('city_destination');
        $total_weight = $this->input->get('total_weight');
        $cost = json_decode($this->rajaongkir->cost(151, $city_destination, $total_weight, $courier));
       
                    
        echo "<table id='cost_table'>
        <thead>
            <tr>
            <th>Pilih</th>
            <th>Servis</th>
            <th>Deskripsi</th>
            <th>Biaya Pengiriman</th>
            <th>Estimasi</th>
            </tr>
        </thead>
        <tbody id='myTable'>";

        foreach($cost->rajaongkir->results as $rows) {
            foreach($rows->costs as $costs){
                echo "<tr><td class='text-center'><input type='radio' class='service' name='service' value=''></td><td class='text-center'>" .$costs->service.  "</td><td class='text-center'>$costs->description </td>";
                foreach($costs->cost as $cost){
                    echo "<td class='text-center' value='$cost->value' >Rp.".number_format($cost->value, 0,'.',',')."</td><td class='text-center'>".$cost->etd."</td><tr>";
                }
            }
            }

        echo "</tbody></table>";
        
    }
}

