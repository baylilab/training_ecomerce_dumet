<?php


/**
 * summary
 */
class MemberLoginController extends My_Controller
{
    /**
     * summary
     */
    public function __construct()
    {
        parent::__construct();
		$this->load->model(array('MemberAuth'));
    }

    public function login()
    {
    	$this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE)
        {
           return redirect('/CheckoutController');
        }
        else
        {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $member   = $this->MemberAuth->get_member_for_login($username);
            
            if(!empty($member)) {
            	if(password_verify($password, $member->password) AND $member->activation == 1) {

            		$this->session->set_userdata(array(

            			'id_member' => $member->id,
            			'name_member' => $member->username,
            			'logged_in' => TRUE 
            		));
            	  
            	   return redirect('CheckoutController');

	            } else {
	            	die('login gagal atau anda belum melakukan aktifasi akun anda melalui email');
	            }
            }

            return redirect('CheckoutController');
        }
    }

    public function logoutMember() 
    {
    	$this->session->sess_destroy();

    	return redirect('CheckoutController');
    }

}