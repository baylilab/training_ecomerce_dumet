<?php

class MemberRegisterController extends My_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model(array('MemberAuth'));
    }

    public function register()
    {
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('shipping_address', 'Shipping_address', 'required');
        $this->form_validation->set_rules('telp', 'Telp', 'required');

        $name = $this->input->post("name");
        $username = $this->input->post("username");
        $password = $this->input->post("password");
        $email = $this->input->post("email");
        $shipping_address = $this->input->post("shipping_address");
        $telp = $this->input->post("telp");
        $member_token = random_string('alnum', 32);

        if ($this->form_validation->run() == FALSE)
        {
           $this->session->set_flashdata('register_fail', 'Registrasi Gagal');
            die('Gagal register');
        }
        else
        {
                $data = array(
                    'member_name'=> $name,
                    'email'     => $email,
                    'telp'      => $telp,
                    'shipping_address'  => $shipping_address,
                    'username'  => $username,
                    'password'  => password_hash($password, PASSWORD_BCRYPT),
                    'member_token' => $member_token,
                    'activation' => 0,
                    'datetime'  => date('Y-m-d H:i:s'), 

                );

                    $config = array();
                    $config['charset'] = 'utf-8';
                    $config['useragent'] = 'Codeigniter';
                    $config['protocol']= "smtp";
                    $config['mailtype']= "html";
                    $config['smtp_host']= "ssl://smtp.googlemail.com";//pengaturan smtp
                    $config['smtp_port']= "465";
                    $config['smtp_timeout']= "400";
                    $config['smtp_user']= "amadeus.bayu@gmail.com"; // isi dengan email kamu
                    $config['smtp_pass']= "osiris051jigsaw12"; // isi dengan password kamu
                    $config['crlf']="\r\n"; 
                    $config['newline']="\r\n"; 
                    $config['wordwrap'] = TRUE;
                    //memanggil library email dan set konfigurasi untuk pengiriman email
                
                    $this->email->initialize($config);
                    //konfigurasi pengiriman
                    $this->email->from($config['smtp_user']);
                    $this->email->to($email);
                    $this->email->subject("Verifikasi Akun");
                    $this->email->message(
                    "terimakasih telah melakuan registrasi, untuk memverifikasi silahkan klik tautan dibawah ini<br><br>".
                    site_url("auth/MemberRegisterController/verfication/$member_token")
                    );

                    if($this->email->send())
                    {
                        $this->MemberAuth->memberRegister($data);
                        echo "Berhasil melakukan registrasi, silahkan cek email kamu <a href=".site_url().">Kemabli ke website</a> ";
                    }else
                    {
                        echo "Gagal mengirim verifikasi email";
                    }

        }


        
    }

        public function verfication()
            {
                $token = $this->uri->segment(4);
                $this->MemberAuth->changeActivation($token);
                echo "Selamat kamu telah memverifikasi akun kamu";
                echo "<br><br><a href='".site_url("/")."'>Kembali ke Menu Login</a>";
            }
}