<?php

class ShoppingCartController extends My_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Home','ShopCart'));
	}

	public function index()
	{
		$page_path    = "pages/cart";
		$cart_content = $this->cart->contents();
		$cart_total   = $this->cart->total(); 
		$latest_product  = $this->Home->latestProduct();
		$low_price_product = $this->Home->lowPriceProduct();
		
		$this->load->view('base', compact('page_path','cart_content','cart_total','latest_product','low_price_product'));
	}

	public function directAddToCart($id)
	{
		$data_product = $this->ShopCart->getProductByid($id);
		
			$data = array(
                'id'      => $data_product->id,
                'qty'     => 1,
                'price'   => $data_product->price,
                'name'    => $data_product->product_name,
				'img'     => $data_product->thumbnail, 
				'weight'     => $data_product->weight,
            );

     		$this->cart->insert($data);
	
		
	
		return redirect('/');

		
	}

	public function addToCartWithForm()
	{

		$data_product = $this->ShopCart->getProductByid($this->input->post('id'));
		
			$data = array(
                'id'      => $this->input->post('id'),
                'qty'     => $this->input->post('qty'),
                'price'   => $data_product->price,
                'name'    => $data_product->product_name,
				'img'     => $data_product->thumbnail, 
				'weight'     => $data_product->weight,
            );

     		$this->cart->insert($data);
	
		
	
		return redirect('HomeController/productDetail/'.$this->input->post('id'));

		

		
	}

	public function editCartItem()
	{
		if(isset($_POST['update_cart'])) {

			$rowid = $this->input->post('rowid');
		    $qty   = $this->input->post('qty');


		 for($i=0; $i<=count($this->cart->contents()); $i++) {
			$data = array(
                'rowid'      => $rowid[$i],
                'qty'        => $qty[$i],
                );

     		$this->cart->update($data);

	 }

		return redirect('ShoppingCartController');
		
		} else {
			return  redirect('CheckoutController');
		}
		

	}

	public function removeCartItem($rowid) {   
        $data = array(
            'rowid'   => $rowid,
            'qty'     => 0
        );

        $this->cart->update($data);

        return redirect('ShoppingCartController');
}

}