<?php

/**
 * summary
 */
class CheckoutController extends My_Controller
{
    /**
     * summary
     */
    public function __construct()
    {
		parent::__construct();
        $this->load->model(array('Home','ShopCart','MemberAuth'));
        $this->load->library('RajaOngkir');
       
    }


    public function index()
    {
        $page_path = "pages/checkout";
        $provinces = json_decode($this->rajaongkir->province());    
        $data_member = $this->MemberAuth->get_memeber_by_id($this->session->userdata('id_member'));
        $cart_sub_total = $this->cart->total();
        $total_wight = 0;
        foreach($this->cart->contents() as $item)
        {
        $total_wight += $item['weight'];
        }
        $cart_contents = $this->cart->contents();

    	$this->load->view('base', compact('page_path','data_member','provinces','total_wight','cart_contents','cart_sub_total'));
    }

    
}