<?php

class My_Controller extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();
        $this->load->database();
		$this->load->library(array('cart', 'form_validation', 'pagination', 'session','form_validation','email'));
		$this->load->helper(array('url', 'text', 'form','string'));
		
    }   
}